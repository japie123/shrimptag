# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'yaml'
puts "Loading database.yml......"
news = File.open('database.yml') { |f| YAML.load(f) }
news.each do |eachNews|
	n = News.new
	n.title=eachNews[:title]
	n.desc=eachNews[:desc]
	n.fbcm=eachNews[:shhh] + eachNews[:push] + eachNews[:comments]
	n.fbid=eachNews[:fbid]
  n.aid=eachNews[:aid]
	n.save

  puts n.title
end
