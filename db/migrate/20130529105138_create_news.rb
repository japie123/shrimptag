class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.text :desc
      t.text :fbcm
      t.string :fbid
      t.string :aid
      t.datetime :lastModified, :null=>false, :default => Time.new(1990,1,1)
      t.integer :shrimpSum, :null=>false, :default => 0
      t.integer :sum, :null=>false, :default => 0

      t.timestamps
    end
  end
end
