class RemoveIndexFromNews < ActiveRecord::Migration
  def up
    remove_index :news, :name => 'index_news_on_sum_and_lastModified'
  end
  def down
    add_index :news, ["sum", "lastModified"]
  end
end
