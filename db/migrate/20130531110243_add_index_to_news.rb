class AddIndexToNews < ActiveRecord::Migration
  def change
    add_index :news, ["sum", "lastModified"]
  end
end
