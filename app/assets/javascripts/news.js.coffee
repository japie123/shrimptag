# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

console.log('news.js.coffee!')
newsId = +$('input[name=newsid]').val()

localStorage.shrimpUserId = localStorage.shrimpUserId || Math.random()

if newsId
	$.post('http://shrimptaglogger.herokuapp.com/log', {
		newsid: newsId,
		timestamp: "" + new Date(),
		location: location.href,
		userid: "" + localStorage.shrimpUserId
		}, ((data) -> console.log(data)), 'json')

$('button').click ()->
	v = $(this).data('value')
	$(this).parent().append('<input type="text" value="'+v+'" style="visibility:hidden" name="label">')

