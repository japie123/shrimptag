class NewsController < ApplicationController

	before_filter :redirect_to_ended, :except => :ended

	def edit
		@oneNews = News.where('"sum"<3 AND "lastModified"<?',Time.now-1200).order('sum').first
		@oneNews.lastModified = Time.now
		@oneNews.save
	end
	def update
		# puts params[:newsid]
		@target = News.where('id = ?',params[:newsid].to_i)[0]
		@target.lastModified = Time.new(1990,1,1)
		if params[:label] != 'donotKnow'
			@target.sum = @target.sum + 1
			# puts @target.id,": ",@target.sum
			if params[:label] == 'Shrimp'
				@target.shrimpSum = @target.shrimpSum + 1
			end
			@target.save
		end
		redirect_to onePost_path
	end
	def example
	end

	def ended
		render 'end'
	end

private
	def redirect_to_ended
		redirect_to ended_path
	end
end
