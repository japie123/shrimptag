class News < ActiveRecord::Base
  attr_accessible :desc, :fbcm, :fbid, :title
  serialize :fbcm, Array
end
